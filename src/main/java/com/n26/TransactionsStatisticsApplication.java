package com.n26;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class TransactionsStatisticsApplication {

    public static void main(String... args) {
        SpringApplication.run(TransactionsStatisticsApplication.class, args);
    }

}
