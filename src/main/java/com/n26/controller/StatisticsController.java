package com.n26.controller;

import com.n26.service.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.n26.model.Statistics;

/**
 * Rest Controller for requests /statistics
 * @author  Himanshu Garg
 * @version 1.0
 */
@RestController
@RequestMapping("statistics")
public class StatisticsController {

	private final Logger logger = LoggerFactory.getLogger(StatisticsController.class);
	private final StatisticsService statisticsService;

	@Autowired
	public StatisticsController(StatisticsService statisticsService) {
		this.statisticsService = statisticsService;
	}

	@GetMapping
	public Statistics getStatistics() {
		logger.info("Request received for Statistics");
		return statisticsService.getTransactionStats();
	}

}
