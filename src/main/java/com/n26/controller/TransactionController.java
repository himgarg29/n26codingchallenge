package com.n26.controller;

import com.n26.exceptions.FutureDateTransactionException;
import com.n26.exceptions.OldTransactionException;
import com.n26.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.n26.model.Transaction;
import java.util.Date;

/**
 * Rest Controller for requests /transactions
 * @author  Himanshu Garg
 * @version 1.0
 */
@RestController
@RequestMapping("transactions")
public class TransactionController {

	@Value( "${transaction.old.max.limit:60}" )
	private int oldTransactionMaxLimit;
	private final Logger logger = LoggerFactory.getLogger(TransactionController.class);

	private final TransactionService transactionService;

	@Autowired
	public TransactionController(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void createTransaction(@RequestBody Transaction transaction) throws FutureDateTransactionException, OldTransactionException {

		Date requestReceivedTimestamp = new Date();
		logger.info("Transaction received with timestamp: {}",transaction.getTimestamp());

		if(transaction.getTimestamp().after(requestReceivedTimestamp))
			throw new FutureDateTransactionException("Transaction date is in future",transaction.getTimestamp());
		if((requestReceivedTimestamp.getTime() - transaction.getTimestamp().getTime())/1000 >= oldTransactionMaxLimit)
			throw new OldTransactionException("Transaction is older than 60 seconds",transaction.getTimestamp());

		transactionService.createTransaction(transaction);
		logger.info("Transaction with timestamp: {} successfully added",transaction.getTimestamp());
	}
	
	@DeleteMapping
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteTransactions() {
		logger.info("Request received to delete all transactions");
		transactionService.deleteAllTransactions();
		logger.info("All transactions deleted");
	}
}
