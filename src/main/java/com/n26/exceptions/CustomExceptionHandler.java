package com.n26.exceptions;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Consolidate multiple Exceptions into a single, global error handling component.
 * @author  Himanshu Garg
 * @version 1.0
 */
@ControllerAdvice
public class CustomExceptionHandler {

	private final Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);

	@ResponseStatus(HttpStatus.NO_CONTENT)
	    @ExceptionHandler({OldTransactionException.class})
	    public void oldTransaction(OldTransactionException exp) {
		logger.info("{} : {}",exp.getMessage(),exp.getTimestamp());
	    }
	 @ResponseStatus(HttpStatus.BAD_REQUEST)
	    @ExceptionHandler({JsonParseException.class,MismatchedInputException.class})
	    public void invalidJsonRequest(Exception exp) {
		logger.error(exp.getMessage());

	    }
	 @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	    @ExceptionHandler({InvalidFormatException.class})
	    public void invalidInput(Exception exp) {
		logger.error(exp.getMessage());
	    }

	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler({FutureDateTransactionException.class})
	public void futureDateTransaction(FutureDateTransactionException exp) {
		logger.info("{} : {}",exp.getMessage(),exp.getTimestamp());
	}
}
