package com.n26.exceptions;

import lombok.Getter;
import java.util.Date;

/**
 * Custom Exception, thrown when the transaction's timestamp is in future
 * @author  Himanshu Garg
 * @version 1.0
 */
@Getter
public class FutureDateTransactionException extends Exception{

    private final Date timestamp;

    public FutureDateTransactionException(String message, Date timestamp)
    {
        super(message);
        this.timestamp = timestamp;
    }
}
