package com.n26.exceptions;

import lombok.Getter;
import java.util.Date;

/**
 * Custom Exception, thrown when the transaction's timestamp is older than the specified time
 * @author  Himanshu Garg
 * @version 1.0
 */
@Getter
public class OldTransactionException extends Exception{

    private final Date timestamp;

    public OldTransactionException(String message, Date timestamp)
    {
        super(message);
        this.timestamp = timestamp;
    }
}