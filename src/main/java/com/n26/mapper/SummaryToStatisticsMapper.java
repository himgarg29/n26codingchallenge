package com.n26.mapper;

import com.n26.model.Statistics;

import java.math.BigDecimal;
import java.util.DoubleSummaryStatistics;

public class SummaryToStatisticsMapper {

    private SummaryToStatisticsMapper(){}
    public static Statistics mapToStatistics(DoubleSummaryStatistics summaryStatistics){
        return Statistics.builder()
                .sum(BigDecimal.valueOf(summaryStatistics.getSum()))
                .max(BigDecimal.valueOf(summaryStatistics.getMax()))
                .min(BigDecimal.valueOf(summaryStatistics.getMin()))
                .avg(BigDecimal.valueOf(summaryStatistics.getAverage()))
                .count(summaryStatistics.getCount())
                .build();
    }
}
