package com.n26.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.n26.utils.BigDecimalSerializer;
import lombok.*;
import java.math.BigDecimal;

/**
 * Represents a Statistics data model that is sent to the client in response to a request.
 * @author  Himanshu Garg
 * @version 1.0
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Statistics {

	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal sum = BigDecimal.ZERO;

	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal avg = BigDecimal.ZERO;

	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal max = BigDecimal.ZERO;

	@JsonSerialize(using = BigDecimalSerializer.class)
	private BigDecimal min = BigDecimal.ZERO;

	private long count;
}
