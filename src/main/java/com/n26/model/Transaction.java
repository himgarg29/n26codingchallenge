package com.n26.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Represents a Transaction data model that is received from the client.
 * @author  Himanshu Garg
 * @version 1.0
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction implements Comparable<Transaction> {

	private BigDecimal amount;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX", timezone = "UTC")
    private Date timestamp;

	@Override
	public int compareTo(Transaction transaction) {
		return this.timestamp.compareTo(transaction.getTimestamp());
	}
}
