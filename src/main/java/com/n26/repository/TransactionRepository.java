package com.n26.repository;

import com.n26.model.Transaction;

import java.util.List;

public interface TransactionRepository {
     boolean createTransaction(Transaction transaction);
     void deleteAllTransactions();
     List<Transaction> getAllTransaction();
     void removeOldTransactions();
}
