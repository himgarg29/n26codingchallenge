package com.n26.repository.impl;

import com.n26.model.Transaction;
import com.n26.repository.TransactionRepository;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.stream.Collectors;

@Repository
public class TransactionRepositoryImpl implements TransactionRepository {

    private int thresholdTimeInSec;
    private final Logger logger = LoggerFactory.getLogger(TransactionRepositoryImpl.class);
    BlockingQueue<Transaction> transactionsQueue = new PriorityBlockingQueue<>();

    public TransactionRepositoryImpl(@Value("${transaction.remove.threshold.time:60}") int thresholdTimeInSec){
        this.thresholdTimeInSec = thresholdTimeInSec;
    }

    @Override
    public boolean createTransaction(Transaction transaction) {
        return transactionsQueue.add(transaction);
    }

    @Override
    public void deleteAllTransactions() {
        transactionsQueue.clear();
    }

    @Override
    public List<Transaction> getAllTransaction() {

        return transactionsQueue.parallelStream().collect(Collectors.toList());
    }

    @Override
    @Scheduled(fixedRateString = "${transaction.remove.polling.interval:10}")
    public void removeOldTransactions() {

        Date thresholdTime = DateUtils.addSeconds(new Date(), -1*thresholdTimeInSec);
        while(!transactionsQueue.isEmpty()){
            Transaction probeTransaction = transactionsQueue.peek();
            if(probeTransaction.getTimestamp().compareTo(thresholdTime)<0) {
                transactionsQueue.poll();
                logger.info("Transaction with timestamp: {} deleted, as it was older than threshold limit: {}",probeTransaction.getTimestamp(),thresholdTime);
                }
            else
                break;

        }
    }
}
