package com.n26.service;

import com.n26.model.Transaction;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TransactionService {

    boolean createTransaction(Transaction transaction);
    void deleteAllTransactions();
    List<Transaction> getAllTransactions();
}