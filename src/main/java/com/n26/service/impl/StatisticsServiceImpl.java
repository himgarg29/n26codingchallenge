package com.n26.service.impl;

import com.n26.mapper.SummaryToStatisticsMapper;
import com.n26.model.Statistics;
import com.n26.model.Transaction;
import com.n26.service.StatisticsService;
import com.n26.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    private TransactionService transactionService;

    @Autowired
    public StatisticsServiceImpl(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Statistics getTransactionStats() {

        List<Transaction> transactionList = transactionService.getAllTransactions();
        if(transactionList.isEmpty())
            return new Statistics();

        DoubleSummaryStatistics summary = transactionList.parallelStream()
                .collect(Collectors.summarizingDouble(transaction -> transaction.getAmount().doubleValue()));

        return SummaryToStatisticsMapper.mapToStatistics(summary);
    }
}
