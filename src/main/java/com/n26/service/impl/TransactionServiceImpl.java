package com.n26.service.impl;

import com.n26.model.Transaction;
import com.n26.repository.TransactionRepository;
import com.n26.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public boolean createTransaction(Transaction transaction) {
        return transactionRepository.createTransaction(transaction);
    }

    @Override
    public void deleteAllTransactions() {
        transactionRepository.deleteAllTransactions();
    }

    @Override
    public List<Transaction> getAllTransactions() {
        return transactionRepository.getAllTransaction();
    }




}
