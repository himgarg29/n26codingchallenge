package com.n26.controller;

import com.n26.model.Statistics;
import com.n26.service.StatisticsService;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StatisticsController.class)
public class StatisticsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private StatisticsService statisticsService;

    public StatisticsControllerTest() {
    }

    @Test
    @SneakyThrows
    public void testGetStatistics(){
        Statistics statistics = Statistics.builder()
                .avg(BigDecimal.valueOf(0,2))
                .max(BigDecimal.valueOf(0,2))
                .min(BigDecimal.valueOf(0,2))
                .sum(BigDecimal.valueOf(0,2))
                .count(0)
                .build();

        given(statisticsService.getTransactionStats()).willReturn(statistics);
        mvc.perform(get("/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"sum\": \"0.00\", \"avg\": \"0.00\", \"max\":\"0.00\", \"min\":\"0.00\", \"count\":0}"));

        verify(statisticsService).getTransactionStats();
    }
}
