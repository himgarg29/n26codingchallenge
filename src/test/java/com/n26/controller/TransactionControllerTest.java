package com.n26.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.model.Transaction;
import com.n26.service.TransactionService;
import lombok.SneakyThrows;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TransactionService transactionService;

    @SneakyThrows
    @Test
    public void testDeleteTransactions(){
        doNothing().when(transactionService).deleteAllTransactions();
          mvc.perform(delete("/transactions"))
                .andExpect(status().isNoContent());
        verify(transactionService).deleteAllTransactions();
    }

    @SneakyThrows
    @Test
    public void testCreateTransactionsInvalidJson(){
        given(transactionService.createTransaction(any())).willReturn(true);
        mvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"timestamp\"::::\"2018-07-17T09:59:51.312Z\", \"amount\":\"12.3343\"}"))
                .andExpect(status().isBadRequest());

        verify(transactionService, never()).createTransaction(any());
    }

    @SneakyThrows
    @Test
    public void testCreateTransactionsOldTransaction(){
        given(transactionService.createTransaction(any())).willReturn(true);
        ObjectMapper objectMapper = new ObjectMapper();
        Transaction transaction = objectMapper.readValue(new File("src/test/resources/validtransactionrequest.json"), Transaction.class);
        Date validTime = DateUtils.addSeconds(new Date(), -61);
        transaction.setTimestamp(validTime);

        String request = objectMapper.writeValueAsString(transaction);
        mvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(status().isNoContent());

        verify(transactionService, never()).createTransaction(any());
    }

    @SneakyThrows
    @Test
    public void testCreateTransactionsFutureTransaction(){
        given(transactionService.createTransaction(any())).willReturn(true);
        ObjectMapper objectMapper = new ObjectMapper();
        Transaction transaction = objectMapper.readValue(new File("src/test/resources/validtransactionrequest.json"), Transaction.class);
        Date validTime = DateUtils.addSeconds(new Date(), 1);
        transaction.setTimestamp(validTime);

        String request = objectMapper.writeValueAsString(transaction);
        mvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(status().isUnprocessableEntity());

        verify(transactionService, never()).createTransaction(any());
    }

    @SneakyThrows
    @Test
    public void testCreateTransactionsFieldNotParsable(){
        given(transactionService.createTransaction(any())).willReturn(true);
        String request = Files.readString(Paths.get("src/test/resources/notparsabletransactionrequest.json"));

        mvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(status().isUnprocessableEntity());

        verify(transactionService, never()).createTransaction(any());
    }

    @SneakyThrows
    @Test
    public void testCreateTransactions(){
        given(transactionService.createTransaction(any())).willReturn(true);
        ObjectMapper objectMapper = new ObjectMapper();
        Transaction transaction = objectMapper.readValue(new File("src/test/resources/validtransactionrequest.json"), Transaction.class);
        Date validTime = DateUtils.addSeconds(new Date(), -30);
        transaction.setTimestamp(validTime);

        String request = objectMapper.writeValueAsString(transaction);

        mvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                .andExpect(status().isCreated());

        verify(transactionService).createTransaction(any());
    }

}
