package com.n26.mapper;

import com.n26.model.Statistics;
import org.junit.Assert.*;
import org.junit.Test;

import java.util.DoubleSummaryStatistics;
import java.util.stream.DoubleStream;

import static org.junit.Assert.assertEquals;

public class SummaryToStatisticsMapperTest {

    @Test
    public void testMapToStatistics(){

        DoubleSummaryStatistics summary = DoubleStream.of(1, 2, 3).summaryStatistics();
        Statistics stats = SummaryToStatisticsMapper.mapToStatistics(summary);

        assertEquals(summary.getAverage(),stats.getAvg().doubleValue(),0);
        assertEquals(summary.getSum(),stats.getSum().doubleValue(),0);
        assertEquals(summary.getMax(),stats.getMax().doubleValue(),0);
        assertEquals(summary.getMin(),stats.getMin().doubleValue(),0);
        assertEquals(summary.getCount(),stats.getCount(),0);

    }
}
