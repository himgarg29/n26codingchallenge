package com.n26.repository;

import com.n26.repository.impl.TransactionRepositoryImpl;
import com.n26.utils.TestObjectBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class TransactionRepositoryTest {

    private TransactionRepository transactionRepository;
    private int thresholdTimeInSec = 5;

    @Before
    public void setUp(){
        transactionRepository = new TransactionRepositoryImpl(thresholdTimeInSec);
    }

    @Test
    public void testCreateTransactions(){
        transactionRepository.createTransaction(TestObjectBuilder.getTransactionObject());
        assertThat(transactionRepository.getAllTransaction(), hasSize(1));
    }

    @Test
    public void testGetAllTransactions(){
        transactionRepository.createTransaction(TestObjectBuilder.getTransactionObject());
        transactionRepository.createTransaction(TestObjectBuilder.getTransactionObject());
        assertThat(transactionRepository.getAllTransaction(), hasSize(2));
    }

    @Test
    public void testDeleteAllTransactions(){
        transactionRepository.createTransaction(TestObjectBuilder.getTransactionObject());
        assertThat(transactionRepository.getAllTransaction(), hasSize(1));
        transactionRepository.deleteAllTransactions();
        assertThat(transactionRepository.getAllTransaction(), hasSize(0));

    }

    @Test
    public void testRemoveOldTransactions(){

        transactionRepository.createTransaction(TestObjectBuilder.getTransactionObject());
        transactionRepository.createTransaction(TestObjectBuilder.getTransactionObject());

        try {
            transactionRepository.removeOldTransactions();
            assertThat(transactionRepository.getAllTransaction(), hasSize(2));

            Thread.sleep(thresholdTimeInSec*2000);
            transactionRepository.removeOldTransactions();
            assertThat(transactionRepository.getAllTransaction(), hasSize(0));
     } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
