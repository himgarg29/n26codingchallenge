package com.n26.service;

import com.n26.model.Statistics;
import com.n26.model.Transaction;
import com.n26.service.impl.StatisticsServiceImpl;
import com.n26.utils.TestObjectBuilder;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsServiceTest {

    @Mock
    private TransactionService transactionService;

    @InjectMocks
    private StatisticsService statisticsService = new StatisticsServiceImpl(transactionService);

    @Test
    public void testGetTransactionStats(){
        List<Transaction> transactionList = new ArrayList<>();
        Transaction transaction = TestObjectBuilder.getTransactionObject();
        transaction.setAmount(BigDecimal.valueOf(10.13445));
        transactionList.add(transaction);

        transaction = TestObjectBuilder.getTransactionObject();
        transaction.setAmount(BigDecimal.valueOf(3.8583));
        transactionList.add(transaction);

        given(transactionService.getAllTransactions()).willReturn(transactionList);
        Statistics stats = statisticsService.getTransactionStats();

        assertEquals(BigDecimal.valueOf(6.996375),stats.getAvg());
        assertEquals(BigDecimal.valueOf(10.13445),stats.getMax());
        assertEquals(BigDecimal.valueOf(3.8583),stats.getMin());
        assertEquals(BigDecimal.valueOf(13.99275),stats.getSum());
        assertEquals(transactionList.size(),stats.getCount());
        verify(transactionService).getAllTransactions();


    }
}
