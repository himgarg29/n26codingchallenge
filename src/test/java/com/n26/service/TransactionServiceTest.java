package com.n26.service;

import com.n26.model.Transaction;
import com.n26.repository.TransactionRepository;
import com.n26.service.impl.TransactionServiceImpl;
import com.n26.utils.TestObjectBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private final TransactionService transactionService = new TransactionServiceImpl(transactionRepository);

    @Test
    public void testCreateTransaction(){

        Transaction transaction = TestObjectBuilder.getTransactionObject();
        given(transactionRepository.createTransaction(transaction)).willReturn(true);

        assertTrue(transactionService.createTransaction(transaction));
        verify(transactionRepository).createTransaction(transaction);
    }

    @Test
    public void testDeleteAllTransaction(){

        doNothing().when(transactionRepository).deleteAllTransactions();
        transactionService.deleteAllTransactions();
        verify(transactionRepository).deleteAllTransactions();
    }

    @Test
    public void testGetAllTransaction(){

        List<Transaction> transactionList = TestObjectBuilder.getTransactionList();
        given(transactionRepository.getAllTransaction()).willReturn(transactionList);

        assertThat(transactionRepository.getAllTransaction(),hasSize(transactionList.size()));
        verify(transactionRepository).getAllTransaction();
    }
}
