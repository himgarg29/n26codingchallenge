package com.n26.utils;

import com.n26.model.Transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestObjectBuilder {

    public static Transaction getTransactionObject(){
        Transaction transaction = Transaction.builder()
                .amount(BigDecimal.valueOf(1.000))
                .timestamp(new Date())
                .build();
        return transaction;
    }
    public static List<Transaction> getTransactionList(){
        List<Transaction> transactionList = new ArrayList<>();
        transactionList.add(getTransactionObject());
        transactionList.add(getTransactionObject());
        return transactionList;
    }
}
